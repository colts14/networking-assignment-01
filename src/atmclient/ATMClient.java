package atmclient;

import java.awt.EventQueue;
import dto.*;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.Font;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Component;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import java.awt.Color;

public class ATMClient {

	private JFrame frmAtmSimulation;
	private JTextField txtAccount;
	private JTextField txtPin;
	private JLabel lblPin;
	private JLabel lblMessage;
	private JTextField txtAmount;
	private JLabel lblAmount;
	private JLabel lblSwitchDesc;
	private JPanel panel_1;
	private JPanel panel_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ATMClient window = new ATMClient();
					window.frmAtmSimulation.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ATMClient() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAtmSimulation = new JFrame();
		frmAtmSimulation.getContentPane().setBackground(new Color(100, 149, 237));
		frmAtmSimulation.setTitle("ATM Simulation");
		frmAtmSimulation.setBounds(100, 100, 396, 286);
		frmAtmSimulation.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		txtAccount = new JTextField();
		txtAccount.setColumns(10);

		JLabel lblAccount = new JLabel("Account");
		lblAccount.setForeground(new Color(255, 215, 0));
		lblAccount.setLabelFor(txtAccount);

		txtPin = new JTextField();
		txtPin.setAlignmentX(Component.RIGHT_ALIGNMENT);
		txtPin.setColumns(10);

		lblPin = new JLabel("Pin");
		lblPin.setForeground(new Color(255, 215, 0));
		lblPin.setLabelFor(txtPin);
		lblPin.setAlignmentX(Component.RIGHT_ALIGNMENT);

		JButton btnDeposit = new JButton("Deposit");
		btnDeposit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runTransaction(btnDeposit.getText());
			}
		});

		JButton btnWithdraw = new JButton("Withdraw");
		btnWithdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runTransaction(btnWithdraw.getText());
			}
		});

		txtAmount = new JTextField();
		txtAmount.setAlignmentX(Component.RIGHT_ALIGNMENT);
		txtAmount.setColumns(10);

		lblAmount = new JLabel("Amount");
		lblAmount.setForeground(new Color(255, 215, 0));
		lblAmount.setLabelFor(txtAmount);
		
		JButton btnBalance = new JButton("Balance");
		btnBalance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runTransaction(btnBalance.getText());
			}
		});
		
		panel_1 = new JPanel();
		panel_1.setBackground(new Color(100, 149, 237));
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		
		panel_2 = new JPanel();
		panel_2.setBackground(new Color(100, 149, 237));
		FlowLayout flowLayout_1 = (FlowLayout) panel_2.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		GroupLayout groupLayout = new GroupLayout(frmAtmSimulation.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblPin)
								.addComponent(lblAccount)
								.addComponent(lblAmount))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(txtPin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtAmount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtAccount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnWithdraw, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnDeposit, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnBalance, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE))
					.addGap(21))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(260, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnDeposit, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnWithdraw, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnBalance, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblAccount)
								.addComponent(txtAccount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblPin)
								.addComponent(txtPin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblAmount)
								.addComponent(txtAmount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(29, Short.MAX_VALUE))
		);
		
				lblSwitchDesc = new JLabel("");
				lblSwitchDesc.setForeground(new Color(255, 215, 0));
				panel_2.add(lblSwitchDesc);
				lblSwitchDesc.setFont(new Font("Tahoma", Font.BOLD, 12));
		
				lblMessage = new JLabel("");
				lblMessage.setForeground(new Color(255, 255, 255));
				panel_1.add(lblMessage);
				lblMessage.setFont(new Font("Tahoma", Font.PLAIN, 12));
		frmAtmSimulation.getContentPane().setLayout(groupLayout);
	}

	/**
	 * Validate fields
	 */
	public Boolean validateFields(){
		try{
			Integer.parseInt(txtAccount.getText());
			Integer.parseInt(txtPin.getText());
			if (txtAmount.getText().trim().equals("")) {
				txtAmount.setText("0.00");
			}
			Double.parseDouble(txtAmount.getText());
		} catch (NumberFormatException e){
			return false;
		}
		return true;
	}
	
	public void runTransaction(String action) {

		// validate fields first
		Boolean areValid = validateFields();
		
		if (!areValid) {
			lblSwitchDesc.setText("Error");
			lblMessage.setText("<html>One or more fields is not valid<br>Please verify that fields are correct");
			return; // exit the method
		}
		
		// Guard everything in a try-finally to make
		// sure that the socket is closed:
		try {
			InetAddress addr = InetAddress.getByName(null);
			Socket socket = new Socket(addr, 8080);
			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(socket.getInputStream());

			AccountDTO oAccDTO = new AccountDTO(Integer.parseInt(txtAccount.getText()),
					Integer.parseInt(txtPin.getText()), Double.parseDouble(txtAmount.getText()));
			oAccDTO.setOperation(action);
			out.writeObject(oAccDTO);
			out.flush();
			ResultDTO oResult = (ResultDTO) in.readObject();

			if (oResult.getMessage() != null) {
				lblSwitchDesc.setText("Error");
				lblMessage.setText(oResult.getMessage());
			} else {
				lblSwitchDesc.setText("Balance");
				lblMessage.setText(String.format("$%,.2f", oResult.getBalance()));
			}

			out.close();
			in.close();
			socket.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
