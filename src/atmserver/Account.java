package atmserver;
// Account.java
// Represents a bank account

public class Account 
{
   private int accountNumber; // account number
   private int pin; // PIN for authentication
   private double totalBalance; // funds available

   // Account constructor initializes attributes
   public Account(int theAccountNumber, int thePIN, double theTotalBalance)
   {
      accountNumber = theAccountNumber;
      pin = thePIN;
      totalBalance = theTotalBalance;
   } // end Account constructor

   // determines whether a user-specified PIN matches PIN in Account
   public boolean validatePIN(int userPIN)
   {
      if (userPIN == pin)
         return true;
      else
         return false;
   } // end method validatePIN
   
   // returns the total balance
   public double getTotalBalance()
   {
      return totalBalance;
   } // end method getTotalBalance

   // deposits an amount to the account
   public void deposit(double amount)
   {
      totalBalance += amount; // add to total balance
   } // end method deposit

   // withdraws an amount from the account
   public void withdraw(double amount)
   {
      totalBalance -= amount; // subtract from total balance
   } // end method withdraw

   // returns account number
   public int getAccountNumber()
   {
      return accountNumber;  
   } // end method getAccountNumber
} // end class Account
